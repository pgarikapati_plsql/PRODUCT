SELECT qp_price_list_line_util.get_product_value
                                              ('QP_ATTR_DEFNS_PRICING',
                                               qppr.product_attribute_context,
                                               qppr.product_attribute,
                                               qppr.product_attr_value
                                              ),
       a.NAME, qppr.product_uom_code,                      -- qpll.list_price,
                                     qpll.operand item_price,
       qpll.inventory_item_id,
       DECODE (UPPER (qppr.product_attr_value),
               'ALL', NULL,
               qppr.product_attr_value
              )
  FROM qp_list_headers_all a,
       qp_list_lines qpll,
       qp_pricing_attributes qppr,
       mtl_system_items mtl
--WHERE a.list_header_id = 533750
WHERE  a.list_header_id = qpll.list_header_id
   AND qppr.product_attribute_context = 'ITEM'
   AND TO_NUMBER (qppr.list_line_id) = qpll.list_line_id
   AND TO_NUMBER (qppr.product_attr_value) = mtl.inventory_item_id
   AND mtl.organization_id = (SELECT qp_util.get_item_validation_org
                                FROM DUAL)
   AND mtl.segment1 = '&ITEM'
   AND qpll.list_line_type_code IN ('PLL', 'PBH')
   AND qppr.pricing_phase_id = 1
   AND qppr.qualification_ind IN (4, 6, 20, 22)
   AND qpll.pricing_phase_id = 1
   AND qpll.qualification_ind IN (4, 6, 20, 22)
